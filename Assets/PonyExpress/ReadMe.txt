*** 
Go to    http://newmoonlegacy.azurewebsites.net   for more information about Pony Express
***


Pony Express Overview:

1) Add the /PonyExpress/ClientSettings.cs script to the Main Camera in the first scene of your application

*** This will register the network component with your application ***
*** The network component will stay active for the life of your application ***



2) Set up your Network Server

Unzip PonyExpress/DemoServer/DemoServer.zip, and open the project using any edition of Visual Studio.  
You can download and install Visual Studio Express for free via www.visualstudio.com

Run the DemoServer on the machine which will be your Network Server - Or move the necessary network code into your existing network server if you already have an existing project you are working with.



3) *** Optional ***
Create a custom Message Processor and define custom messages.
An example (default) message processor is provided - DefaultMessaging.cs which you can use as a template when creating your own - or just use DefaultMessaging.cs



4) When you are ready to connect (either onStart, or maybe in a button click event) -

   A) Initialize the MessageProcessor
   B) Call ClientSettings.Connect with the MessageProcessor you initialized


There are two Connect methods - one with username and password, and one without.
You may choose to include a username and password that will be sent to your server, or call the Connect method without username and password if you are not using Pony Express for authentication.

Send the MessageProcessor you initialized to the Connect Method.


SampleMainMenu/SampleMainMenu.cs - line 55 is an example of steps 3 & 4 in action -
_clientSettings.Connect(_serverIP, _serverPort, "TEST", "CorrectHorseBatteryStaple", new DefaultMessaging());




Watch a YouTube Video showing the toolkit in action:
https://www.youtube.com/watch?v=TSVFEN2Isxc



Visit http://NewMoonLegacy.AzureWebsites.net for documentation on the Pony Express classes



****

Pony Express Explained:

The Networking folder has several files:

ClientSettings.cs -
   This is the class that is responsible for connecting your clients to your server.  Connection state is managed with this object, and the object will persist throughout the life of the application until the Shutdown() method is called.  Incoming messages and outgoing messages will use this class to communicate with the server.


ProcessMessageBase.cs -
   This abstract base class defines exposes the correct patterns to follow to create your own Messaging class.  Any class you use to define your messages should inherit from this base class.


DefaultMessaging.cs -
   This is an example class demonstrating how messages can be sent to the server and read back from the server.  The static methods:  WhoAmI() and SendChat() are used in SampleMainMenu.cs and ChatClient.cs to show how messages can be sent to the server from your application's code.  The ProcessServerNetworkMessage() fires every time a message comes from the server, and should call whatever part of your application you want when new server messages come in.  Please view the Pony Express documentation at http://newmoonlegacy.azurewebsites.net for more information on creating and reading messages to and from the server.


ServerNetworkMessage.cs -
   This serves as our contract between the client and server, defining the structure of messages.  All messages are sent as UTF-8, and are pipe-delimited string arrays.  The first string in the message is always the unique Message Type ID, telling the client or server how to parse the message it is currently processing.


ChatClient.cs -
   This is the sample Chat Client utility.  Attach this script to your main camera to add chat to your application.




The SampleMainMenu  has one file:

SampleMainMenu.cs -
   This is an example of a scene that can be followed to implement the Pony Express networking utility.  See a YouTube video with this class in action at: https://www.youtube.com/watch?v=TSVFEN2Isxc




The Plugins folder added to the root of your project has several files inside.  These are necessary for network connectivity with iOS and Android devices.  Please e-mail NewMoonLegacy@gmail.com with any questions or concerns you may have any of these components.


****

Sample Server Explained:

When the DemoServer starts, Program.cs executes the Main() routine.  

In this routine, the DemoServer starts two threads:

MessageServer.cs - ConnectionListener

This thread listens for new connections to the server.  When a new connection comes in, first Authentication.CheckAuthentication passes in the optional username and password fields from the client.  In Demo server, Authentication.CheckAuthentication is currently set to always return true.

After the client has been successfully authenticated, an asynchronous task is started (LoginPlayer) - to connect the client with the server.  Messages are sent and received asynchronously so that the server can communicate with several clients at the same time.



MessageServer.cs - UpdateLoop

This thread is a starting point for your server code.  Any necessary background processes for your server should be put into this thread.  A video game server may want to respawn enemies on this thread, or any other time-sensitive operations that are not based on messages to and from the client.



Reading Messages on the Server from Clients:

When a client that has registered with the server sends a message, the Message Server calls the HandleMessage function asynchronously.

MessageServer.cs - HandleMessage


This method parses the message from the Unity client, and processes it appropriately.  In Demo Server, there are two messages already defined:  ClientChat & WhoAmI.  In both instances, the DemoServer wants to immediately send out messages to respond to the requests from the clients.





Sending Messages on the Server to Clients:

Two sample messages are defined on the Demo Server:  

MessageServer.cs - SendWhoAmI()

On the game server, every connected "Player" has a unique Guid that helps to distinguish them from the others.  This is a suggested implementation for the client application level.  On the game server, we use the unique connection handle exposed at the socket level to know to whom the message belongs - but this is kept secret from the client for security reasons.  

At the client we can use the Guid to track which specific client might be involved in a message containing information for several "players".



MessageServer.cs - SendChatMessage()

In the sample client, we have a sample chat library.  When a chat message is sent to the Demo Server, we want to broadcast that Chat Message to every connected player.  In DemoServer, the player name is coming from the username provided during the login process.  This could be modified to come from a custom message, or some other location entirely.  See https://newmoonlegacy.azurewebsites.net for more information on creating new messages and troubleshooting issues you may come across.





Confused about any step?  Curious where to go now?  Send an email to:  newmoonlegacy@gmail.com

*** 
Please go to    http://newmoonlegacy.azurewebsites.net   for more information about Pony Express
***