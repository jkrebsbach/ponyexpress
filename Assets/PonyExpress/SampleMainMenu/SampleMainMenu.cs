﻿using UnityEngine;
using System.Collections;
using System;

public class SampleMainMenu : MonoBehaviour {
	
    private ClientSettings _clientSettings;

    // This should be the IP of the server
    private string _serverIP = "192.168.0.22";



    // What Port is the server running on?
    private int _socketServerPort = 5768;
    private int _webSocketServerPort = 8765;


    private bool _connectingClicked = false;
    private bool _connectionResolved = false;
	private DateTime _whoAmICheck = DateTime.MinValue;
    private string _statusMessage = string.Empty;
    
    // Use this for initialization
	void Start () {
        // Start the game manager (Client Settings) as soon as the app opens
        _clientSettings = FindObjectOfType<ClientSettings> ();
	}

    
	// Update is called once per frame
	void OnGUI () {
        if (_statusMessage != string.Empty)
        {
            // Inform the user how the connection is comming along
			GUI.Label (new Rect (50, 50, 450, 50), _statusMessage);
		}

        if (GUI.Button(new Rect(Screen.width - 80, Screen.height - 60, 50, 50), "QUIT"))
        {
            Application.Quit();
        }


        if (_connectionResolved)
        {
            // No further action necessary on this screen
        }
        else
        {
            if (!_connectingClicked)
            {
                var connectButtonHeight = Screen.height/5;

                if (GUI.Button(new Rect(Screen.width / 2 - (connectButtonHeight / 2), Screen.height / 2 - (connectButtonHeight / 2), 
                    connectButtonHeight, connectButtonHeight), "CONNECT"))
                {
                    _connectingClicked = true;
                    _statusMessage = "OPENING CONNECTION...";


                    // Send the Messaging Library you want to use - either extend the default, or create your own
                    StartCoroutine(
                        _clientSettings.Connect(_serverIP, _socketServerPort, _webSocketServerPort, "TEST", "CorrectHorseBatteryStaple",
                            new DefaultMessaging())
                        );

                    // Connect your messaging library to the network server - you'll want this to talk back and forth
                    DefaultMessaging.ClientSettings = _clientSettings;
                }

            }
            else
            {

                _statusMessage = "SIGNING IN...";

                // Check every few seconds to see if the login process on the remote server has completed
				if (_clientSettings.IsConnected && _clientSettings.WhoAmI == Guid.Empty && _whoAmICheck < _clientSettings.ServerTime)
                {
                    DefaultMessaging.WhoAmI();
					_whoAmICheck = _clientSettings.ServerTime.AddSeconds(2);
                }
                else if (_clientSettings.LoginFailure)
                {
                    _statusMessage = "Having problems logging in.  Please try again later.";
                    _connectingClicked = false;
                }
                else if (_clientSettings.WhoAmI != Guid.Empty)
                {
                    // We are now connected - Move to the next seen or whatever else needs to happen
                    _statusMessage = "CONNECTED!!!";
                    _connectionResolved = true;

                    // Application.LoadLevel ("OverworldScene");
                }

            }
        }
	}
}