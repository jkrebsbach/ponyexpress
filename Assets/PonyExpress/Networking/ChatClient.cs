﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChatClient : MonoBehaviour {

	private struct MessageEntity {
		public float MessageTime;
		public string MessageContent;
	}

    private string _chatInput = "";
    private float _lastScreenWidth = 0f;

    public GUISkin chatterSkin;

    private Vector2 _chatOutputSize;
    private Vector2 _chatOutputPosition;
    private Vector2 _chatInputSize;
    private Vector2 _chatInputPosition;

    private Vector2 _chatScroll = Vector2.zero;

    private Rect _chatInputRect;
    private Rect _chatOutputRect;
    private Rect _entireChatArea;
    private Rect _chatSendRect;

    private List<MessageEntity> _messageBuffer = new List<MessageEntity>();
    private ClientSettings _clientSettings = null;

	// Use this for initialization
	protected void Start () {
		_clientSettings = FindObjectOfType<ClientSettings>();

		if (_clientSettings == null)
			throw new UnityException("Can't find Client Settings - did you add it to your primary scene?");
	}
	
	// Update is called once per frame
	protected void OnGUI () {
        AdjustScale();

        if (_clientSettings.IsConnected)
        {
            Rect chatOutputContents = new Rect(0, 0, _chatOutputSize.x, _messageBuffer.Count * 20);

            GUI.Box(_chatOutputRect, string.Empty);

            _chatScroll = GUI.BeginScrollView(_chatOutputRect, _chatScroll, chatOutputContents);

            for (int index = 0; index < _messageBuffer.Count; index++)
            {
                GUI.Label(new Rect(0, index * 20, _chatOutputSize.x, 20), _messageBuffer[index].MessageContent);
            }

            GUI.EndScrollView();

            _chatInput = GUI.TextField(_chatInputRect, _chatInput);
            if (GUI.Button(_chatSendRect, "SEND"))
            {
                SendChat(_chatInput);
                _chatInput = string.Empty;
            }
        }
	}

	private void SendChat(string message)
	{
		NetOutgoingMessage om = new NetOutgoingMessage();
		om.Write(((long)DefaultMessaging.NetworkPackage.ClientChat).ToString());
		om.Write(message);
		_clientSettings.SendMessage(om);
	}

    // Mobile device rotation will affect screen width - just one option for watching screen size change
    private void AdjustScale()
    {
        if (Screen.width != _lastScreenWidth)
        {
            _lastScreenWidth = Screen.width;

            var chatButtonheight = Screen.height/5;

            _chatOutputSize = new Vector2(325, 175);
            _chatOutputPosition = new Vector2(0, Screen.height - 175 - chatButtonheight);

            _chatInputSize = new Vector2(350, chatButtonheight);
            _chatInputPosition = new Vector2(0, Screen.height - chatButtonheight);

            _chatInputRect = new Rect(_chatInputPosition.x, _chatInputPosition.y, _chatInputSize.x, _chatInputSize.y);
            _chatSendRect = new Rect(_chatInputPosition.x + _chatInputSize.x + 20, _chatInputPosition.y, 100, _chatInputSize.y);
            _chatOutputRect = new Rect(_chatOutputPosition.x, _chatOutputPosition.y, _chatOutputSize.x + 25, _chatOutputSize.y);
            _entireChatArea = new Rect(_chatInputPosition.x, _chatInputPosition.y, Mathf.Max(_chatInputSize.x, _chatOutputSize.x), Mathf.Max(_chatInputSize.y, _chatOutputSize.y));
        }
    }

    // Preserve the chat client between scenes
    void Awake()
	{
		DontDestroyOnLoad (transform.gameObject);
	}

	public void ProcessMessage(string[] pieces) {
        string player = pieces[1];
        string message = pieces[2];

        _messageBuffer.Add(new MessageEntity()
        {
            MessageTime = System.DateTime.Now.Ticks,
            MessageContent = string.Format("{0}: {1}", player, message)
        });
        _chatScroll = new Vector2(0, Mathf.Infinity); // Scroll to bottom when new message arrives
	}

}