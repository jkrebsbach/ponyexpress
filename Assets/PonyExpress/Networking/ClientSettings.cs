using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Threading;

#if NETFX_CORE
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Networking;
using Windows.Networking.Sockets;
using Windows.Storage;
using Windows.Storage.Streams;
#elif UNITY_EDITOR_WIN || UNITY_EDITOR_OSX || UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX || UNITY_WEBPLAYER
using System.Net.Sockets;
using System.Net;

#endif
public class NetOutgoingMessage
{
		public String Data = String.Empty;
		private String delim = String.Empty;
		public System.Object SenderEndpoint;

		public void Write (System.String output)
		{ 
				Data = String.Format ("{0}{1}{2}", Data, delim, output);
				delim = "|";
		}
}

public class ClientSettings : MonoBehaviour
{
#region Private Member Variables
	private Guid _whoAmI = Guid.Empty;
	private bool _connected = false;
	private string _ipAddress = null;
	private int _port = 0;
    private ProcessMessageBase _messageProcessor = null;
	private bool _connecting = false;

	private ChatClient _chatClient;

    #endregion

#if UNITY_WEBGL
    private static WebSocket _socket = null;
#elif UNITY_ANDROID
	private static AndroidJavaClass AndroidConnection = null;
#elif UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX || UNITY_WEBPLAYER 
    private static Socket _socket = null;
#elif NETFX_CORE
	private StreamSocket _socket = null;
#elif UNITY_IPHONE

#endif


    #region Public Properties

    public Stack<String[]> UpdateNPCQueue = new Stack<String[]> ();

	public Guid WhoAmI {
			get {
					return _whoAmI;
			}
	}

	// Default Socket Policy Port is 843
	public int SocketPolicyPort = 843;
	public bool LoginFailure = false;

	private static Queue<ServerNetworkMessage> _serverNetworkMessageQueue = new Queue<ServerNetworkMessage> ();
	private static int _messageCount = 0;
	private NetOutgoingMessage _connectMessage;

#endregion

	// Use this for initialization
	void Start()
	{
		
#if UNITY_ANDROID
		AndroidJNI.AttachCurrentThread();
		AndroidConnection = new AndroidJavaClass ("com.newmoonlegacy.ponyexpress.UnityBridge");
#elif UNITY_IPHONE

#endif

        _chatClient = FindObjectOfType<ChatClient> ();
	}

	void OnApplicationQuit() {
		Disconnect (null);
	}

	void Awake ()
	{
		// ClientSettings - network utility should never dispose, even when switching scenes
		DontDestroyOnLoad (transform.gameObject);
	}

	// Update is called once per frame
	void Update ()
	{
		if (!_connected && _connecting) {


            try
            {

#if UNITY_WEBPLAYER
                bool isOK = Security.PrefetchSocketPolicy (_ipAddress, SocketPolicyPort);
				if (!isOK) {
					Debug.Log("Unable to fetch socket policy for Web Player");
				}

				Debug.Log(String.Format("Attempting to connect to {0}:{1}", _ipAddress, _port));
#endif

#if UNITY_ANDROID
			    object[] connectionInfo = new object[] {
					    _ipAddress,
					    _port,
					    name,
					    "androidMessageCallback"
			    };

            	String result = AndroidConnection.CallStatic<String>("Connect", connectionInfo);
				
				result = AndroidConnection.CallStatic<String>("ReadValues");

#elif UNITY_WEBGL

                if (_socket.error != null)
                    throw new Exception(_socket.error);

#elif UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX || UNITY_WEBPLAYER 

                _socket = new Socket (AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
				_socket.Connect (IPAddress.Parse (_ipAddress), _port);
				ReadValues();
#elif NETFX_CORE

				var connTask = MetroConnect();
				connTask.Wait();
				var task = ReadValues();


#elif UNITY_IPHONE
        		SocketClient.InitNetworkCommunication(_ipAddress, (uint)_port);
#endif

                SendMessage (_connectMessage);

	            _connected = true;
		
            }
            catch (Exception ex)
            {
                Shutdown();
                Debug.Log(ex.Message);
				Debug.Log (ex.StackTrace);
            }

		}

#if UNITY_ANDROID || UNITY_IPHONE || UNITY_WEBGL
		string newMessage = CheckMessages ();	
	    
		if (newMessage != null) {
            foreach(String messageLine in newMessage.Split('\n')) {
			    MessageHandler (messageLine.Split ('|'));
            }
		}
#endif

        
        if (_connected) {
						
						lock (this) {
								while (_serverNetworkMessageQueue.Count > 0) {
										_messageCount--;
										ServerNetworkMessage message = _serverNetworkMessageQueue.Dequeue ();
                                        _messageProcessor.ProcessServerNetworkMessage(message);
								}
						}
				}
	    }


    public IEnumerator Connect (string ipAddress, int socketServerPort, int webSocketServerPort, ProcessMessageBase messageProcessor) 
	    {
		    yield return Connect (ipAddress, socketServerPort, webSocketServerPort, string.Empty, string.Empty, messageProcessor);
	    }

	    public IEnumerator Connect (string ipAddress, int socketServerPort, int webSocketServerPort, string username, string password, ProcessMessageBase messageProcessor)
	    {
            LoginFailure = false;
            _messageProcessor = messageProcessor;
		    _ipAddress = ipAddress;
		    
            if (_connectMessage == null)
            {
                _connectMessage = new NetOutgoingMessage();
                _connectMessage.Write(username);
                _connectMessage.Write(password);
            }

#if UNITY_WEBGL
	        _port = webSocketServerPort;
            yield return StartCoroutine(WebSocketConnect());
#else
            _port = socketServerPort;
#endif

        _connecting = true;
            yield return "Ready";

    }

#if UNITY_WEBGL

    private IEnumerator WebSocketConnect()
    {
        _socket = new WebSocket(new Uri(string.Format("ws://{0}:{1}", _ipAddress, _port)));
        yield return StartCoroutine(_socket.Connect());
        _connecting = true;
    }

#endif

    public String SendMessage (NetOutgoingMessage om)
	    {
            try
            {
#if UNITY_ANDROID
			    if (AndroidConnection != null) {
				    String result = AndroidConnection.CallStatic<String>("SendMessage", om.Data + "~");
			    }

#elif UNITY_EDITOR_WIN || UNITY_EDITOR_OSX || UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX || UNITY_WEBPLAYER || UNITY_WEBGL
                Byte[] buffer = System.Text.Encoding.UTF8.GetBytes(om.Data + "~");
                _socket.Send(buffer);
#elif NETFX_CORE
			    var task = MetroSend(om);
			    task.Wait();

#elif UNITY_IPHONE
                string result = SocketClient.SendMessage(om.Data + "~");
#endif
            }
            catch(Exception ex)
            {
                Shutdown();
                Debug.Log(ex.Message);
                return "Error on send";
            }
		    return "Send";
	    }
	
		public String CheckMessages ()
		{
			String result = null;
#if UNITY_ANDROID
		    if (AndroidConnection != null) {
			    result = AndroidConnection.CallStatic<String>("CheckMessages");
		    }
#elif UNITY_IPHONE
                result = SocketClient.CheckMessages();
#elif UNITY_WEBGL
            if (_socket != null) {
		        var bytes = _socket.Recv();
                if (bytes != null)
                    result = Encoding.UTF8.GetString(bytes);
            }
#endif
            return result;
		}
	
		public bool IsConnected 
		{
			get
			{
				return _connected;
			}
		}
	
		public void Disconnect (string options)
		{
#if UNITY_ANDROID
			AndroidConnection = null;
#elif UNITY_EDITOR_WIN || UNITY_EDITOR_OSX || UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX || UNITY_WEBPLAYER || UNITY_WEBGL
        if (_socket != null)
				_socket.Close();
#elif NETFX_CORE
		if (_socket != null) {
			_socket.Dispose();
			_socket = null;
		}
#elif UNITY_IPHONE
            SocketClient.StopSocket();
#endif
		}

#if UNITY_ANDROID

   

    
#elif UNITY_EDITOR_OSX || UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX || UNITY_WEBPLAYER

    private const int READ_BUFFER_SIZE = 1024;
	private byte[] readBuffer = new byte[READ_BUFFER_SIZE];
		
	public void ReadValues() {
		EndPoint remoteEndPoint = _socket.RemoteEndPoint;
			
		_socket.BeginReceiveFrom(readBuffer, 0, READ_BUFFER_SIZE, SocketFlags.None, ref remoteEndPoint, new AsyncCallback(DoRead), null);
	}
		
	private void DoRead(IAsyncResult ar)
	{
		int bytesRead = 0;
		try
		{
			bytesRead = _socket.EndReceive(ar);
			if (bytesRead < 1)
			{
				// Server closed
				return;
			}
				
			string message = System.Text.Encoding.UTF8.GetString(readBuffer, 0, bytesRead);
			foreach(String messageLine in message.Split('\n')) {
				MessageHandler(messageLine.Split ('|'));
			}

			EndPoint remoteEndPoint = _socket.RemoteEndPoint;
			// Start a new async read process
			_socket.BeginReceiveFrom(readBuffer, 0, READ_BUFFER_SIZE, SocketFlags.None, ref remoteEndPoint, new AsyncCallback(DoRead), null);
		}
		catch
		{
				
		}
	}

#elif NETFX_CORE

	private async Task<int> MetroConnect() {
		HostName host = new HostName(_ipAddress);
		_socket = new StreamSocket();
		await _socket.ConnectAsync(host, _port.ToString(), SocketProtectionLevel.PlainSocket);

		return 1;
	}

	private async Task<int> MetroSend(NetOutgoingMessage om) {
		Byte[] buffer = System.Text.Encoding.UTF8.GetBytes (om.Data + "~");
		DataWriter writer = new DataWriter(_socket.OutputStream);
		//uint len = writer.MeasureString(om.Data + "~");
		
		writer.WriteBytes(buffer);
		await writer.StoreAsync();
        await writer.FlushAsync();
		writer.DetachStream();
		writer.Dispose();

		return 1;
	}

	private const int READ_BUFFER_SIZE = 1024;
	private Windows.Storage.Streams.Buffer readBuffer = new Windows.Storage.Streams.Buffer(READ_BUFFER_SIZE);

	public async Task<int> ReadValues()
	{
		IAsyncOperationWithProgress<Windows.Storage.Streams.IBuffer, uint> readOp = _socket.InputStream.ReadAsync(readBuffer, READ_BUFFER_SIZE, InputStreamOptions.Partial);
		readOp.Completed = (IAsyncOperationWithProgress<IBuffer, uint>
		                    asyncAction, AsyncStatus asyncStatus) =>
		{
			switch (asyncStatus)
			{
			case AsyncStatus.Completed:
			case AsyncStatus.Error:
				try
				{
					// GetResults in AsyncStatus::Error is called as it throws a user friendly error string.
					IBuffer localBuf = asyncAction.GetResults();
					uint bytesRead = localBuf.Length;
					var readPacket = DataReader.FromBuffer(localBuf);
					OnDataReadCompletion(bytesRead, readPacket);
				}
				catch (Exception exp)
				{
					System.Diagnostics.Debug.WriteLine("Read operation failed:  " + exp.Message);
				}
				break;
			case AsyncStatus.Canceled:
				
				// Read is not cancelled in this sample.
				break;
			}
		};

		return 1;
	}
	
	private void OnDataReadCompletion(uint bytesRead, DataReader readPacket)
	{
		if (readPacket == null)
		{
			System.Diagnostics.Debug.WriteLine("DataReader is null");
			
			// Ideally when read completion returns error, 
			// apps should be resilient and try to 
			// recover if there is an error by posting another recv
			// after creating a new transport, if required.
			return;
		}
		uint buffLen = readPacket.UnconsumedBufferLength;
		System.Diagnostics.Debug.WriteLine("bytesRead: " + bytesRead + ", unconsumedbufflength: " + buffLen);
		
		// check if buffLen is 0 and treat that as fatal error.
		if (buffLen == 0)
		{
			System.Diagnostics.Debug.WriteLine("Received zero bytes from the socket. Server must have closed the connection.");
			System.Diagnostics.Debug.WriteLine("Try disconnecting and reconnecting to the server");
			return;
		}
		
		// Perform minimal processing in the completion
		string message = readPacket.ReadString(buffLen);
		System.Diagnostics.Debug.WriteLine("Received Buffer : " + message);
		
		foreach (String messageLine in message.Split('\n'))
		{
			MessageHandler(messageLine.Split('|'));
		}
		
		// Post another receive to ensure future push notifications.
		ReadValues();
	}

#endif


    /// <summary>
    /// Handles messages - can happen out of sync with update thread, so uses queue to keep updates in sync
    /// with UI
    /// </summary>
    /// <param name="pieces">Pieces.</param>
    private void MessageHandler (string[] pieces)
		{
			int messageNum = 0;
			if (pieces.Length > 0 && pieces[0] != String.Empty && Int32.TryParse(pieces[0], out messageNum)) {

				int packID = Int32.Parse (pieces [0]); // read long.
				ServerNetworkMessage message = new ServerNetworkMessage ()
				{
					PackID = packID,
					Pieces = pieces
				};

				lock (this) {
						_serverNetworkMessageQueue.Enqueue (message);
						_messageCount++;
				}
			}
		}

	public void UpdateWhoAmI (string[] msg)
	{
		string myID = msg [1];

		if (_whoAmI == Guid.Empty) {
				_whoAmI = new Guid (msg [1]);
		}
		
        Debug.Log ("We are : " + myID);
	}

    public void ProcessMessage(string[] msg)
    {
        _chatClient.ProcessMessage(msg);
    }

    public void Shutdown()
    {
        LoginFailure = true;
        _ipAddress = null;
        _connected = false;
		_connecting = false;

#if UNITY_ANDROID

#elif UNITY_WEBGL
        if (_socket != null)
            _socket.Close();
        _socket = null;

#elif UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX || UNITY_WEBPLAYER
        if (_socket != null)
            _socket.Disconnect(false);
        _socket = null;
#endif
        Debug.Log("RECEIVED REQUEST TO SHUTDOWN");
    }

	// It is a good idea to connect this property to the remote server so that all clients can know what time it should be
	public DateTime ServerTime
	{
		get
		{
			return DateTime.Now;
		}
	}
}