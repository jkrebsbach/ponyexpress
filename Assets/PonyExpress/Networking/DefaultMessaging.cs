﻿using System;
using System.Collections;
using UnityEngine;

public class DefaultMessaging : ProcessMessageBase {

    public enum NetworkPackage
	{
        // Connection Packages 		1xxx
		WhoAmI				= 1001,
		ShutDown			= 1002,
        
		// Client Chat Packages		2xxx
		ClientChat			= 2001,
	}
	
	#region Updates for the server

	public static void WhoAmI()
	{
		NetOutgoingMessage om = new NetOutgoingMessage ();
		om.Write (((long)NetworkPackage.WhoAmI).ToString ());
		ClientSettings.SendMessage (om);
	}

	public static void SendChat(string message)
	{
		NetOutgoingMessage om = new NetOutgoingMessage();
		om.Write(((long)NetworkPackage.ClientChat).ToString());
		om.Write(message);
		ClientSettings.SendMessage(om);
	}


	#endregion

    public override void ProcessServerNetworkMessage(ServerNetworkMessage message)
    {
        if (Enum.IsDefined(typeof(NetworkPackage), message.PackID))
        {
            NetworkPackage networkPackage = (NetworkPackage)message.PackID;

            switch (networkPackage)
            {
                case NetworkPackage.WhoAmI:
                    ClientSettings.UpdateWhoAmI(message.Pieces);
                    break;
                case NetworkPackage.ClientChat:
                    ClientSettings.ProcessMessage(message.Pieces);
                    break;
                case NetworkPackage.ShutDown:
                    ClientSettings.Shutdown();
                    break;
            
            }
        }
        else
        {
            Debug.Log(String.Format("Unexpected message ID: {0}", message.PackID));
        }
    }
}
