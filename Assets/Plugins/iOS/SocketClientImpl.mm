
#import "SocketClientImpl.h"

@implementation SocketClientDelegate

NSInputStream *inputStream;
NSOutputStream *outputStream;

NSMutableArray * messages;

- (id)init
{
    self = [super init];
    return self;
}

- (void)dealloc
{
    [super dealloc];
}

- (void)stop
{
    // Disconnect?
}

- (void)initNetworkCommunication:(NSString *)ipAddress port:(UInt32)port{
    CFReadStreamRef readStream;
    CFWriteStreamRef writeStream;
    
    messages = [[NSMutableArray alloc] init];
    
    CFStreamCreatePairWithSocketToHost(NULL, (__bridge CFStringRef)ipAddress, port, &readStream, &writeStream);
    inputStream = (__bridge NSInputStream *)readStream;
    outputStream = (__bridge NSOutputStream *)writeStream;
    
    [inputStream setDelegate:self];
    [outputStream setDelegate:self];
    
    [inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    
    [inputStream open];
    [outputStream open];
}

- (void)sendMessage:(NSString *)message {
    NSData *data = [[NSData alloc] initWithData: [message dataUsingEncoding:NSASCIIStringEncoding]];
	[outputStream write:(const uint8_t *)[data bytes] maxLength:[data length]];
}

- (void)stream:(NSStream *)theStream handleEvent:(NSStreamEvent)streamEvent {
    switch (streamEvent) {
            
		case NSStreamEventOpenCompleted:
			NSLog(@"Stream opened");
			break;
            
		case NSStreamEventHasBytesAvailable:
            if (theStream == inputStream) {
                
                uint8_t buffer[1024];
                int len;
                
                while ([inputStream hasBytesAvailable]) {
                    len = [inputStream read:buffer maxLength:sizeof(buffer)];
                    if (len > 0) {
                        
                        NSString *output = [[NSString alloc] initWithBytes:buffer length:len encoding:NSASCIIStringEncoding];
                        
                        if (nil != output) {
                            [messages addObject:output];
                            NSLog(@"server said: %@", output);
                        }
                    }
                }
            }
			break;
            
		case NSStreamEventErrorOccurred:
			NSLog(@"Can not connect to the host!");
			break;
            
		case NSStreamEventEndEncountered:
			break;
            
		default:
			NSLog(@"Unknown event");
	}
}

- (NSString *)checkMessages {
    id result = [messages firstObject];
    if (result) {
        [messages removeObject:result];
    }
    return result;
}

@end

static SocketClientDelegate* delegateObject = nil;

// Converts C style string to NSString
NSString* CreateNSString (const char* string)
{
	if (string)
		return [NSString stringWithUTF8String: string];
	else
		return [NSString stringWithUTF8String: ""];
}

// Helper method to create C string copy
char* MakeStringCopy (const char* string)
{
	if (string == NULL)
		return NULL;
	
	char* res = (char*)malloc(strlen(string) + 1);
	strcpy(res, string);
	return res;
}

// When native code plugin is implemented in .mm / .cpp file, then functions
// should be surrounded with extern "C" block to conform C function naming rules
extern "C" {
    
    void _ExternInitNetworkCommunication(const char* ipAddress, UInt32 port)
    {
    	if (delegateObject == nil)
			delegateObject = [[SocketClientDelegate alloc] init];
	    
        [delegateObject initNetworkCommunication:CreateNSString(ipAddress) port:port];
    }
    
	void _ExternSendMessage (const char* message)
	{
		if (delegateObject == nil)
			delegateObject = [[SocketClientDelegate alloc] init];
		
		[delegateObject sendMessage:CreateNSString(message)];
	}
	
	const char* _ExternCheckMessages ()
	{
		// By default mono string marshaler creates .Net string for returned UTF-8 C string
		// and calls free for returned value, thus returned strings should be allocated on heap
		return MakeStringCopy([[delegateObject checkMessages] UTF8String]);
	}
	
    void _ExternStopSocket()
	{
		[delegateObject stop];
	}
	
}


