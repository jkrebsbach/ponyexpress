#import <Foundation/Foundation.h>



@interface SocketClientDelegate : NSObject <NSStreamDelegate>

- (void)initNetworkCommunication : (NSString *)ipAddress port : (UInt32)port;
- (void)sendMessage:(NSString *)message;
- (NSString *)checkMessages;
- (void)stop;

@end

