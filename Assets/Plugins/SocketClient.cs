﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class SocketClient {
#if UNITY_IPHONE

	/* Interface to native implementation */
	
	[DllImport ("__Internal")]
	private static extern void _ExternInitNetworkCommunication (string ipAddress, uint port);
	
	[DllImport ("__Internal")]
	private static extern void _ExternSendMessage (string message);
	
	[DllImport ("__Internal")]
	private static extern string _ExternCheckMessages ();
	
	[DllImport ("__Internal")]
	private static extern void _ExternStopSocket ();
	
	/* Public interface for use inside C# / JS code */
	
	// Open socket connection with remote server
	public static void InitNetworkCommunication(string ipAddress, uint port)
	{
		// Call plugin only when running on real device
		if (Application.platform != RuntimePlatform.OSXEditor)
			_ExternInitNetworkCommunication(ipAddress, port);
	}
	
	// Signal socket streams to close
	public static void StopSocket()
	{
		// Call plugin only when running on real device
		if (Application.platform != RuntimePlatform.OSXEditor)
			_ExternStopSocket();
	}
	
	// Send message to server
	public static string SendMessage(string message)
	{
		// Call plugin only when running on real device
		if (Application.platform != RuntimePlatform.OSXEditor)
		{
			_ExternSendMessage(message);
		}
        return "Success";
	}
	
	// Check to see what messages have queued
	public static string CheckMessages()
	{
		// Call plugin only when running on real device
		if (Application.platform != RuntimePlatform.OSXEditor)
		{
				return _ExternCheckMessages();
		}
			
		return null;
	}

#endif
}
